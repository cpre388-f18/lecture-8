package edu.iastate.jmay.lecture8_raceconditions;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {
    /**
     * This is a list to demonstrate that ArrayLists are *not* thread-safe and may throw exceptions.
     */
    private ArrayList<Integer> list;
    /**
     * This is an integer that demonstrates that some concurrency problems do not raise exceptions
     * directly.
     */
    private int counter;
    /** This is the instance of the TextView to show the result. */
    private TextView counterTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // These two lines are auto-generated and should not be changed.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize default values
        list = new ArrayList<>();
        counter = 0;
        // Locate the instance of the TextView
        counterTextView = findViewById(R.id.counterTextView);
        // Populate the TextView with the default value of count.
        updateLayout();
    }

    /**
     * A helper function that updates all UI components (in this case, just the count TextView).
     */
    private void updateLayout() {
        counterTextView.setText(String.format(Locale.getDefault(), "%d", counter));
    }

    /**
     * This Runnable is intended to be sent as an event to the main/UI thread to update the UI.
     * It's called at the end of the AddMillion class below, which runs on a separate thread.
     */
    private class UpdateUIRunnable implements Runnable {
        @Override
        public void run() {
            updateLayout();
        }
    }

    /**
     * Event handler for the count button.  This spawns one thread that takes about half a second to
     * run.  Clicking the button rapidly spawns multiple threads that overlap in time, demonstrating
     * the concurrency error with integers.
     * @param v the View object that triggered this event (unused, but required)
     */
    public void onCountButtonClick(View v) {
        AddMillion adder = new AddMillion();
        adder.start();
    }

    /**
     * Demo 1: Inconsistent data.  Each thread should add one million to count, but when running
     * concurrently, the actual number is less.
     */
    private class AddMillion extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 1000000; i++) {
                // This next line does not work as expected when running
                counter++;
            }
            // Send an event to the main/UI thread to run updateLayout() to display the result of
            // count.
            counterTextView.post(new UpdateUIRunnable());
        }
    }

    /**
     * Event handler for the list update button click.  This immediately spawns two threads that
     * run concurrently trying to read and modify the same ArrayList.
     * @param v  View object that triggered this event (unused, but required)
     */
    public void onListButtonClick(View v) {
        ListThread lt = new ListThread();
        ListThread lt2 = new ListThread();
        lt.start();
        lt2.start();
    }

    /**
     * Demo 2: Exceptions.  The ArrayList data type is not thread-safe, and throws exceptions when
     * multiple threads try accessing it concurrently.
     */
    private class ListThread extends Thread {
        @Override
        public void run() {
            int total = 0;
            for (int i = 0; i < 1000; i++) {
                // These operations are fine in a single threaded app, but are an error when more
                // than one thread is operating on the list.
                list.add(i);
                // This next line is where the exception will get thrown, but the line above is part
                // of the cause of the error.
                for (Integer j : list) {
                    total += j;
                }
            }
        }
    }
}
